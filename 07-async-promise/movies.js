var MovieService = function () {
  this.movies = null;
  this.moviesPromise = null;
};

MovieService.prototype.getBestMovies = function (count) {
  return this.getMovies().then(function (movies) {
    return movies.slice(0, count);
  });
};

MovieService.prototype.getMovies = function () {

  // TODO 2 - Cachování požadavku

  return fetchMovies();
};

// Načtení dat ze serveru -------------------------------------------------------------------------

function fetchMovies() {
  return fetch("/api/movies").then(function(response){
    return response.json();
  });
}
