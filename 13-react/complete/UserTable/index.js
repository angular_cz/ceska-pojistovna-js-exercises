import React from 'react';
import Header from './Header';
import Row from './Row';

var UserTable = React.createClass({
  propTypes: {
    title: React.PropTypes.string.isRequired,
    users: React.PropTypes.array
  },

  getDefaultProps: function() {
    return {
      users: []
    }
  },

  getInitialState: function() {
    return {
      users: this.props.users
    }
  },

  render: function() {
    var title = this.props.title ? <h2>{this.props.title}</h2> : "";

    var rows = this.state.users.map(this.renderRow_);

    return (
        <div>
          {title}
          <button onClick={this.renewData} className="btn btn-info">Načti data znovu</button>
          <table className="table">
            <thead>
            {/* TODO 1.2 použití komponenty Header v UserTable */}
            <Header />
            </thead>
            <tbody>
            {rows}
            </tbody>
          </table>
        </div>
    );
  },

  renderRow_: function(user) {
    // TODO 2.2 - komponenta Row - předání atributu user
    // TODO 2.3 - komponenta Row - předání atributu key

    // TODO 3.4 - tlačítko smazat - použití

    return <Row user={user} key={user.email} onDelete={this.onDelete} />;
  },

  renewData: function() {
    //  TODO 4.1 - obnovení dat (změna stavu komponenty)
    this.setState( {
      users: this.props.users
    });
  },

  onDelete: function(userToDelete) {
    console.log("onDelete", userToDelete)

    var newUsers = this.state.users.filter(function(user) {
      return user.id !== userToDelete.id;
    });
    console.log(newUsers)
    this.setState({
      users: newUsers
    });

  },
});

export default UserTable;