import React from 'react';

var Row = React.createClass({
  propTypes: {
    user: React.PropTypes.shape({
      name: React.PropTypes.string,
      email: React.PropTypes.string
    }).isRequired,

    // TODO 3.1 - tlačítko smazat - rozšíření rozhraní komponenty
  },

  getDefaultProps: function() {
    return {
      // TODO 3.3 - tlačítko smazat - výchozí handler
    };
  },

  render: function() {
    var user = this.props.user;

    if (!user) {
      return null;
    }

    return <tr>
      <td>{user.name}</td>
      <td>{user.email}</td>
      <td>
        {/* TODO 3.2 - tlačítko smazat - volání handleru */}
        <button className="btn btn-danger">Smazat</button>
      </td>
    </tr>;
  },

  handleClick: function(event) {
    this.props.onDelete(this.props.user);
  }

});

export default Row;