import React from 'react';
import UserTable from './UserTable';

var data = [{
  "id": 1,
  "name": "Petr Novák",
  "email": "petr@novak.cz",
  "archived": false,
  "date": 1440752093789
}, {
  "id": 2,
  "name": "Jaroslav Novák",
  "email": "jarek@novak.cz",
  "archived": false,
  "date": 1440752093789
}, {
  "id": 3,
  "name": "Martin Novák",
  "email": "martin@novak.cz",
  "archived": false,
  "date": 1440752093789
}, {
  "id": 4,
  "name": "Karolína Nováková",
  "email": "karolina@novak.cz",
  "archived": true,
  "date": 1440752093789
}, {
  "id": 5,
  "name": "Tamara Nováková",
  "email": "tamara@novak.cz",
  "archived": false,
  "date": 1440752093789
}];

React.render(<UserTable title="User list" users={data} />, document.getElementById('root'));