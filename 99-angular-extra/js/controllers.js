angular.module('userApp')
  .controller('ListController', function (User, users) {
    this.users = users;

    this.update = function (user) {

      // TODO 3.3 - Uložte záznam
    };

    this.remove = function (user) {
      user.$remove()
        .then(function () {
          var index = this.users.indexOf(user);
          this.users.splice(index, 1);
        }.bind(this));
    };

    this.create = function (userData) {
      var newUser = new User(userData)
      newUser.$save()
        .then(function () {
          this.users.push(newUser);
        }.bind(this));
    }
  })

  // TODO 5.1 - Injektujte službu $location
  .controller('EditController', function (user) {
    this.user = user;

    this.save = function () {
      return this.user.$save()
        // TODO 5.2 - Přesměrujte na seznam kontaktů
    }
  });