// TODO 1.1 - Přidejte závislost na modul pro routing
angular.module('userApp', ['ngResource', 'ngMessages'])
  .config(function ($routeProvider) {

    $routeProvider
      .when('/users', {
        // TODO 1.2 - Přiřaďte routě šablonu

        controller:   'ListController',
        controllerAs: 'list',

        resolve: {
          users: function (User) {
            // TODO 2.2 - Načtěte data ze serveru
          }
        }
      })

      .when('/users/:id', {
        templateUrl:  'edit.html',
        controller:   'EditController',
        controllerAs: 'edit',

        resolve: {
          user: function (User, $route) {
            var id = $route.current.params.id;

            return User.get({'id': id}).$promise;
          }
        }
      })

      .otherwise('/users');
  });
