describe('Odpočet', function() {

  describe("callback", function() {

    // TODO 1.3 - Zrychlete testy
    beforeEach(function() {

    });

    afterEach(function() {

    });

    it('update volá po vteřině (TODO 1.1)', function() {

      // TODO 1.1 - Odstraňte pending
      pending();

      var countdown = new Countdown(1);

      var updateCallback = function(value) {
        // TODO 1.1 - Ověřte snížení hodnoty

      };

      countdown.start(updateCallback);

      // TODO 1.3 - Zrychlete test - Poskočte o 1001
    });

    it('final volá po skončení odpočtu (TODO 1.3)', function(done) {

      // TODO 1.3 - Odstraňte pending
      pending();

      var countdown = new Countdown(3);

      countdown.start(null, function(value) {
        expect(value).toBe(3);
        done();
      });

      // TODO 1.3 - Zrychlete testy - Poskočte o 4000
    });

    it('final nevolá, pokud byl odpočet zastaven (TODO 1.4)', function(done) {

      // TODO 1.4 - Odstraňte pending
      pending();
      var countdown = new Countdown(3);

      countdown.start(null, function() {
        // TODO 1.4 - finalCallback nemá být volán po zastavení odpočtu metodou stop
      });

      expect(countdown.isRunning()).toBe(true);

      countdown.stop();

      jasmine.clock().tick(4000);
      done();
    });

  });

});