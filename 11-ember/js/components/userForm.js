// TODO 1.1 - Připojte komponentu do aplikace
Ember.Component.extend({
  actions: {
    save: function () {

      // TODO 2.3 - odešlete akci z komponenty
    }
  },

  // TODO 2.1 - Vypočítaný atribut invalid
  invalid: function () {
    var validation = this.get('validation');
    return validation.name || validation.email;
  },

  validation: Ember.computed('model.email', 'model.name', function () {
    var errors = {};

    var name = this.get('model.name');
    if (!name) {
      errors.name = 'Musíte zadat jméno';
    }

    var email = this.get('model.email');
    if (!email) {
      errors.email = 'Musíte zadat email.';
    } else {
      var emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if (!emailPattern.test(email)) {
        errors.email = 'Email není platný.';
      }
    }

    return errors;
  })
});