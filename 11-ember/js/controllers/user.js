App.UserController = Ember.Controller.extend({

  actions: {
    saveUser: function (user) {
      user.save().then(function() {
        this.transitionToRoute('users');
      }.bind(this));
    }
  }
});