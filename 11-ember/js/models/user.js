App.User = DS.Model.extend({
  name: DS.attr(),
  email: DS.attr(),
  archived: DS.attr()
});
