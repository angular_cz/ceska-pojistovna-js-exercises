angular.module('userApp')
  // TODO  1.1 - Injektněte načtený seznam do controlleru
  .controller('ListController', function (User) {

    // TODO 1.1 - Předejte seznam do šablony
    this.users = [];

    this.update = function (user) {
      // TODO 3.2 - Uložte záznam uživatele
    };

    this.remove = function (user) {
      user.$remove()
        .then(function () {
          var index = this.users.indexOf(user);
          this.users.splice(index, 1);
        }.bind(this));
    };

    this.create = function (userData) {
      var newUser = new User(userData)
      newUser.$save()
        .then(function () {
          this.users.push(newUser);
        }.bind(this));
    }
  })

  .controller('EditController', function ($location, user) {
    this.user = user;

    this.save = function () {
      return this.user.$save()
        .then(function () {
          $location.path('/');
        });
    }
  });