angular.module('userApp', ['ngRoute', 'ngResource', 'ngMessages'])
  .config(function ($routeProvider) {

    $routeProvider
      .when('/users', {
        templateUrl:  'list.html',
        controller:   'ListController',
        controllerAs: 'list',

        resolve: {
          users: function (User) {
            return User.query().$promise;
          }
        }
      })

      .when('/users/:id', {
        templateUrl:  'edit.html',
        controller:   'EditController',
        controllerAs: 'edit',

        resolve: {
          user: function (User, $route) {
            var id = $route.current.params.id;

            return User.get({'id': id}).$promise;
          }
        }
      })

      .otherwise('/users');
  });
