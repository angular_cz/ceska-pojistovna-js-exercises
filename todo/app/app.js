var TodoCtrl = function ($stateParams, todos, $location, $anchorScroll) {
  this.current = $stateParams.name;

  $anchorScroll("top");

  if (!todos.contains(this.current)) {
    $location.path('/');
  }

  this.template = 'todo-' + this.current;
  this.prev = todos.getPrev(this.current);
  this.next = todos.getNext(this.current);
};

var IndexCtrl = function (todos) {
  this.todos = todos.getTodos();
};

var TodoLoader = function ($http) {
  var todos = [];

  this.initialize = function () {
    return this.loadTodos()
      .then(function (todos) {
        this.todos = todos;
        return this;
      }.bind(this));
  }

  this.loadTodos = function () {
    return $http.get('/todo/todos.json')
      .then(function (response) {
        return response.data;
      })
  }

  this.contains = function (todo) {
    return this.todos.indexOf(todo) > -1;
  }

  this.getTodos = function () {
    return this.todos;
  }

  this.getNext = function (todo) {
    if (!this.contains(todo)) {
      return;
    }
    var index = this.todos.indexOf(todo);
    return this.todos[index + 1];
  }
  this.getPrev = function (todo) {
    if (!this.contains(todo)) {
      return;
    }
    var index = this.todos.indexOf(todo);
    return this.todos[index - 1];
  }
}

function paginationDirective() {
  return {
    restrict: "E",
    templateUrl:"directive-pagination"
  }
}

function solutionDirective() {
  return {
    restrict: "E",
    templateUrl:"directive-solution",
    transclude:true,
    scope : true,
    link:function(scope, elem, attrs) {
      if (attrs.hasOwnProperty('visible')){
        scope.visible = true;
      }

      scope.highlight = 'javascript';
      if (attrs.hasOwnProperty('html')){
        scope.highlight = 'html';
      }

      scope.toggle = function () {
        scope.visible = !scope.visible;
      }
    }
  }
}

function noteDirective() {
  return {
    restrict: "E",
    scope:true,
    templateUrl:"directive-note",
    transclude:true,
    scope : true,
    link:function(scope, elem, attrs) {

      if (attrs.hasOwnProperty('visible')){
        scope.visible = true;
      }

      scope.toggle = function () {
        scope.visible = !scope.visible;
      }
    }
  }
}

angular.module('js-todo', ['ui.router'])
  .service('todoLoader', TodoLoader)
  .directive('pagination', paginationDirective)
  .directive('note', noteDirective)
  .directive('solution', solutionDirective)
  .config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/todo");

    $stateProvider
      .state('todos', {
        url: '/todo',
        abstract:true,
        templateUrl: 'todos',
        controller: IndexCtrl,
        controllerAs: "vm",

        resolve: {
          todos: function (todoLoader) {
            return todoLoader.initialize();
          }
        }
      })
      .state('todos.index', {
        url: '',
        templateUrl: 'intro',
      })
      .state('todos.todo', {
        url: '/todo/:name',

        templateUrl: 'todo',
        controller: TodoCtrl,
        controllerAs: 'todo'
      }
    );
  });
