var jade = require('gulp-jade');
var jadeCompiler = require('jade')
var gulp = require('gulp');
var watch = require('gulp-watch');
var plumber = require('gulp-plumber');
var batch = require('gulp-batch');
var livereload = require('gulp-livereload');
var runSequence = require('run-sequence');
var connect = require('connect');
var serveStatic = require('serve-static');
var fs = require('fs');
var proxy = require('proxy-middleware');
var url = require('url');
var less = require('gulp-less');

function loadTodos() {
  return JSON.parse(fs.readFileSync("./todos.json").toString());
}

var config = {
  httpServer: {
    host: 'localhost',
    port: 8283,
    lrPort: 35730
  },
  proxy:'http://angular-cz-js-api.herokuapp.com/'
}

jadeCompiler.filters.escape = function(block) {
    return block
      .replace(/&/g, '&amp;')
      .replace(/</g, '&lt;')
      .replace(/>/g, '&gt;')
      .replace(/"/g, '&quot;')
      .replace(/#/g, '&#35;')
      .replace(/\\/g, '\\\\');
}


jadeCompiler.filters.escape_ng = function(block) {
  var escaped = jadeCompiler.filters.escape(block);

  return '<span ng-non-bindable>' + escaped + '</span>';
}


gulp.task('jade', function () {
  gulp.src('jade/index.jade')
    .pipe(plumber())
    .pipe(jade({
      jade:jadeCompiler,
      locals: {
        devel: true,
        todos: loadTodos(),
        render: jadeCompiler.renderFile
      }
    }))
    .pipe(gulp.dest('./'))
});

gulp.task('less', function () {
  gulp.src('less/*.less')
    .pipe(plumber())
    .pipe(less())
    .pipe(gulp.dest('css'));
});

gulp.task('less-todo', function () {
  gulp.src('../css/*.less')
    .pipe(plumber())
    .pipe(less())
    .pipe(gulp.dest('../css'));
});

gulp.task('jade-build', function () {
  gulp.src('jade/index.jade')
    .pipe(plumber())
    .pipe(jade({
      jade:jadeCompiler,
      locals: {
        todos: loadTodos(),
        render: jadeCompiler.renderFile
      }
    }))
    .pipe(gulp.dest('../'));
});

gulp.task('watch', function() {
  watch(['jade/*.jade', '../*/complete/todo.jade'], batch(function (events, done) {
    gulp.start('jade', done);
  }));

  watch(['less/*.less'], batch(function (events, done) {
    gulp.start('less', done);
  }));

  watch(['../css/*.less'], batch(function (events, done) {
    gulp.start('less-todo', done);
  }));
})

gulp.task('connect', function () {

  livereload.listen(config.httpServer.lrPort);

  var app = connect();
  app.use(serveStatic('../'));
  app.listen(config.httpServer.port);

  gulp.watch(['index.html','app/*.js', 'css/*.css']).on('change', function (filepath) {
    livereload.changed(filepath, config.httpServer.lrPort);
  });

  app.use('/api', proxy(url.parse(config.proxy)));
});

gulp.task('devel', function () {
  runSequence(
    ['jade', 'connect', 'less'],
    'watch'
  );
});

gulp.task('build', ['jade-build', 'less']);

gulp.task('default', ['devel'])