var app = app || {};

app.UserItemView = Backbone.View.extend({
  tagName: 'tr',
  template: _.template($('#user-template').html()),

  initialize: function () {
    // TODO 4.2 -  Při změně modelu volejte render
    this.listenTo(this.model, 'destroy', this.remove);
  },

  events: {
    // TODO 4.1 - Reakce na změnu checkboxu
    'click .remove': 'destroy'
  },

  destroy: function () {
    // TODO 9.1 - Zavolejte odstranění modelu
  },

  // TODO 9.2 - Odstraňte šablonu
  remove: function () {
    this.$el.remove();
  },

  setArchived: function (e) {
    var checked = this.$('input[name=archived]').is(':checked');
    this.model.setArchived(checked);
  },

  render: function () {
    // TODO 3.1 - Předejte attributy modelu do funkce renderující šablonu
    this.$el.html(this.template());

    this.$el.removeClass('archived');

    if (this.model.get('archived')) {
      this.$el.addClass('archived');
    }
    return this;
  }
});
