var app = app || {};

var AppRouter = Backbone.Router.extend({
  routes: {
    'users': 'list',

    // TODO 5.2 - Definujte routu pro editaci

    '*path': 'otherwise'
  },

  list: function () {
    var listUsers = new app.UserList();

    var listUsersView = new app.ListUsersView({collection: listUsers});
    this.showView_(listUsersView);

    // TODO 2.2 - Načtení ze serveru
  },

  // TODO 5.3 - Předejte id z routy a načtěte odpovídající záznam
  edit: function () {
    var user = new app.User({id: null});

    var editUserView = new app.EditUserView({model: user});
    this.showView_(editUserView);

    user.fetch();
  },
  otherwise: function () {
    Backbone.history.navigate('users');
    this.list();
  },

  showView_: function (view) {

    if (this.currentView) {
      this.currentView.remove();
    }
    this.currentView = view;

    view.render();
    $('#userApp').html(view.el);
  }

});

$(function () {
  app.router = new AppRouter();
  Backbone.history.start();
});

