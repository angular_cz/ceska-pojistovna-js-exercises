var app = app || {};

app.User = Backbone.Model.extend({
  urlRoot: '/api/users',

  defaults: {
    name: '',
    email: '',
    archived: false
  },
  setArchived: function (archived) {
    this.set({'archived': archived});

    // TODO 4.3 - Uložte model
  },
  validate: function (attrs) {
    var errors = null;

    if (!attrs.name) {
      errors = errors || {};
      errors.name = 'Musíte zadat jméno.';
    }

    if (!attrs.email) {
      errors = errors || {};
      errors.email = 'Musíte zadat email.';
    } else {
      var emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if (!emailPattern.test(attrs.email)) {
        errors = errors || {};
        errors.email = 'Email není platný.';
      }
    }

    // TODO 7.1 - Vraťte objekt errors
  }
});
