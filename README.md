## Javascript cvičení - Angular.cz

## Co si nainstalovat
- git - http://git-scm.com/downloads
- aktuální verzi nodeJS - http://nodejs.org/download/

## NPM závislosti
Nodejs se nainstaloval spolu s balíčkovacím nástrojem npm.

## Ověření správnosti instalace

Stáhněte tento balíček kamkoli k sobě. 

```
git clone https://bitbucket.org/angular_cz/ceska-pojistovna-js-exercises
```

Spusťte v jeho kořenovém adresáři následující příkazy, žádný z nich by neměl hlásit chybu (první může chvíli trvat).

```
 npm install

 npm start
```

Nyní, když otevřete prohlížeč na adrese http://localhost:8000/ uvidíte uvítací obrazovku Školení - Javascript - cvičení.
Pokud se stránka zobrazila, je nastylovaná, vlevo vidíte menu, pak je vše v pořádku. Jste na školení připraven.

Pokud došlo během instalace nebo spuštění k nějaké chybě, nebo podívejte se do sekce **Možné problémy**, případně nás kontaktujte na [angular@angular.cz](mailto:angular@angular.cz)

## Použití při cvičeních: ##

Všechny potřebné nástroje a závislosti jste nainstalovali už v předchozích krocích pomocí příkazu "npm install".

### Při dalším spouštění už si tak vystačíte s příkazem: ###
```
npm start
```
který spustí lokální server na adrese http://localhost:8000/

### Spuštění testů v jednotlivých cvičeních ###
V některých cvičeních se testy spouštějí externím nástrojem (karma runner). Spustíte je pomocí následujících příkazů. Příkazy se spouští v kořenovém adresáři balíčku.

#### 07-async-promise ####
```
npm run tests-async
```

#### Dom ####
```
npm run tests-dom
```

### Spuštění cvičení pro React ###
Cvičení v reactu používají pro svůj běh webpack. Je nutné spustit ho následujícím příkazem v kořenovém adresáři balíčku.

#### 13-react ####
```
npm run react-webpack

# pro complete slouží příkaz
npm run react-webpack-complete
```

### Možné probémy ###

####unable to connect to github.com####
pokud vidíte tuto chybovou zprávu po spuštění *npm install*

* máte buď blokováno připojení ke githubu  - to můžete ověřit otevřením github.com v prohlížeči
* nebo máte blokován protokol git - spusťte příkaz, který "přesměruje" protokol git po https

```
git config url."https://github.com/".insteadOf git@github.com:
git config url."https://".insteadOf git://
```

Nyní už by měl příkaz *npm install* fungovat

Pokud problémy přetrvávají, a jste uživatelem systému Windows, může zde být následující problém:

* Nastavení výše se zapíšou do .gitconfig do domovské složky, na Windows s profilem na vzdáleném disku však může každý ze shelů hledat home jinde. Pokud je toto Váš případ, zkopírujte soubor do obou umístění, na sdílenou domovskou složku a c:\users\[name]\

Chcete-li používat konfiguraci i pro další projekty, můžete ji nastavit globálne (přidat atribut --global)

```
git config --global url."https://github.com/".insteadOf git@github.com:
git config --global url."https://".insteadOf git://
```
Nyní už by měl příkaz npm install fungovat.

#### Problém se session na api ####
Nastartujte aplikaci bez podpory session pomocí

```
npm run no-session
```

#### Problém s připojením k serverovému API ####
V případě problémů se serverovým API je možné spustit jej lokálně.
Ve složce *api* je k dispozici java aplikace pro rozhraní, pro případ, že není možné připojit se k vzdálené aplikaci.

Je nutné mít nainstalován *maven*, poté stačí spustit

```
npm run api
```

API se spustí na portu 8080

Http server se cvičeními poté proti tomuto API spusťte pomocí:

```
npm run local
```

nebo 

```
npm run local-no-session
```