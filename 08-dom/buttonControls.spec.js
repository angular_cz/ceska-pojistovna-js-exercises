describe('ButtonControls', function () {

  beforeEach(function () {
    this.buttonsElement = jasmine.createSpyObj('buttonsElement', ['addEventListener']);
    this.display = jasmine.createSpyObj('display', ['setValue', 'clear', 'addToValue', 'getValue']);
    this.calculator = jasmine.createSpyObj('calculator', ['setOperand', 'setOperation', 'clear', 'equals']);

    this.buttons = new App.ButtonControls(this.buttonsElement, this.calculator, this.display);

    this.buttonEvent = {
      target: {
        type: 'button',
        classList: {
          contains: function () {
          }
        },
        getAttribute: function () {
        }
      }
    };
  });

  it('registruje událost click (TODO 2.1)', function () {
    expect(this.buttonsElement.addEventListener).toHaveBeenCalled();
  });

  it('při stisku čísla je aktualizován display (TODO 3.1)', function () {
    spyOn(this.buttonEvent.target, "getAttribute").and.callFake(function (param) {
      console.log(param);
      if (param === "value") {
        return 4;
      }
    });

    this.buttons.processButton(this.buttonEvent);

    expect(this.display.addToValue).toHaveBeenCalledWith(4);

  });

  describe('při stisku operace(TODO 3.2)', function () {
    beforeEach(function () {
      var operationsCheck = function (value) {
        return value === "operation";
      };
      spyOn(this.buttonEvent.target.classList, "contains").and.callFake(operationsCheck);
    });

    it('clear vymaže display i calculator', function () {

      this.buttonEvent.target.name = 'clear';

      this.buttons.processButton(this.buttonEvent);

      expect(this.display.clear).toHaveBeenCalled();
      expect(this.calculator.clear).toHaveBeenCalled();
    });

    it('equals počítá výsledek', function () {

      this.buttonEvent.target.name = 'equals';

      this.buttons.processButton(this.buttonEvent);

      expect(this.calculator.equals).toHaveBeenCalled();
    });

    it('jiné operace se volá setOperation', function () {

      this.buttonEvent.target.name = 'mul';

      this.buttons.processButton(this.buttonEvent);

      expect(this.calculator.setOperation).toHaveBeenCalledWith("mul");
    });

    it('není volána metoda pro změnu čísla', function () {

      this.buttonEvent.target.name = 'plus';

      this.buttons.processButton(this.buttonEvent);

      expect(this.display.addToValue).not.toHaveBeenCalled();
    });
  })

  it('nesmí reagovat na click jiné události (TODO 3.3)', function () {
    this.buttons.processButton({target: {type: 'none'}});

    expect(this.calculator.setOperand).not.toHaveBeenCalled();
    expect(this.calculator.clear).not.toHaveBeenCalled();
    expect(this.display.clear).not.toHaveBeenCalled();
  });

});