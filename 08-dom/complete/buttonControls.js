var App = App || {};

App.ButtonControls = (function () {
  var ButtonControls = function (buttonsElement, calculator, display) {

    this.processButton = function (event) {

      var button = event.target;

      if (button.type !== "button") {
        return;
      }

      if (button.classList.contains("operation")) {
        this.processOperation_(button.name);
      } else {
        var value = button.getAttribute("value");
        this.processNumber_(value);
      }
    }

    this.processOperation_ = function (type) {

      switch (type) {
        case "clear":
          this.calculator.clear();
          this.display.clear();
          break;

        case "equals":

          var result = this.calculator.equals();
          this.display.setValue(result);
          break;

        default:
          this.display.setValue(this.display.getValue());
          this.calculator.setOperand(this.display.getValue());

          this.calculator.setOperation(type);
      }
    }

    this.processNumber_ = function (value) {
      this.display.addToValue(value);
      this.calculator.setOperand(this.display.getValue());
    }

    this.calculator = calculator;
    this.display = display;

    buttonsElement.addEventListener("click", this.processButton.bind(this));
  }

  return ButtonControls;
})();