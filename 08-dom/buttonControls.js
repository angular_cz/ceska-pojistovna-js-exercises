var App = App || {};

App.ButtonControls = (function () {
  var ButtonControls = function (buttonsElement, calculator, display) {

    this.processButton = function (event) {
      // TODO 3 - implementujte reakci na tlačítka

    }

    this.processOperation_ = function (type) {

      switch (type) {
        case "clear":
          this.calculator.clear();
          this.display.clear();
          break;

        case "equals":

          var result = this.calculator.equals();
          this.display.setValue(result);
          break;

        default:
          this.display.setValue(this.display.getValue());
          this.calculator.setOperand(this.display.getValue());

          this.calculator.setOperation(type);
      }
    }

    this.processNumber_ = function (value) {
      this.display.addToValue(value);
      this.calculator.setOperand(this.display.getValue());
    }

    this.calculator = calculator;
    this.display = display;

    // TODO 2.1 - Reagujte na kliknutí
  }

  return ButtonControls;
})();