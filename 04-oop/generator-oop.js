
var generatorFactory = function(initialNumber) {
  initialNumber = initialNumber || 0;
  return function generator() {
    return initialNumber++;
  }
};

// TODO 1.1 - Vytvořte generátor

// TODO 1.2 - Použijte prototype
// TODO 1.3 - Opravte přístup k attributům

// TODO 1.4 - Instancujte Generátor

