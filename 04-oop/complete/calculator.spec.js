
describe('Operace', function () {

  describe('sčítání', function () {
    it('počítá součet', function () {
      var add = new Add();
      expect(add.calculate(1, 2)).toBe(3);
    });

    it('má jméno add', function() {
      var add = new Add();
      expect(add.name).toBe('add');
    });

    it('ošetřuje vstupy', function () {
      var add = new Add();
      expect(add.calculate("1", "2")).toBe(3);
    });
  });

  describe('odčítání', function () {
    it('počítá rozdíl', function () {
      var sub = new Sub();
      expect(sub.calculate(2, 1)).toBe(1);
    });

    it('má jméno sub', function() {
      var sub = new Sub();
      expect(sub.name).toBe('sub');
    });

    it('ošetřuje vstupy', function () {
      var sub = new Sub();
      expect(sub.calculate("2a", "1")).toBe(1);
    });
  });

  describe('využívají dědičnost', function() {
    it('sčítání je potomkem Operation', function(){
      var add = new Add();
      expect(add instanceof Operation).toBeTruthy();
      expect(add instanceof Object).toBeTruthy();

      expect(Operation.prototype.isPrototypeOf(add)).toBeTruthy();
      expect(Object.prototype.isPrototypeOf(add)).toBeTruthy();
    });

    it('sčítání má svůj konstruktor', function(){
      var add = new Add();
      expect(add.constructor).toBe(Add);
    });

    it('odčítání je potomkem Operation', function(){
      var sub = new Sub();
      expect(sub instanceof Operation).toBeTruthy();
      expect(sub instanceof Object).toBeTruthy();

      expect(Operation.prototype.isPrototypeOf(sub)).toBeTruthy();
      expect(Object.prototype.isPrototypeOf(sub)).toBeTruthy();
    });

    it('odčítání má svůj konstruktor', function(){
      var sub = new Sub();
      expect(sub.constructor).toBe(Sub);

    });
  });

});

describe('Kalkulačka', function () {

  it('může být instancována', function () {
    expect((new Calculator()) instanceof Calculator).toBeTruthy();
  });

  it('může mít přidánu operaci pod jménem', function () {
    var calc = new Calculator();
    var operation = new Operation();
    operation.name = 'op';

    calc.addOperation(operation);

    expect(calc.operations['op']).toBe(operation);
  });

  it('umožuje provolat operaci', function () {
    var calc = new Calculator();
    var operation = new Operation();
    operation.name = 'op';

    spyOn(operation, "calculate").and.returnValue(42);

    calc.addOperation(operation);

    expect(calc.calculate('op', 40, 2)).toBe(42);
    expect(operation.calculate).toHaveBeenCalledWith(40,2);
  });

  describe('má instanci calculator, která', function () {
    it('představuje kalkulačku', function () {
      expect(calculator instanceof Calculator).toBeTruthy();
    });

    it('má akci pro sčítání', function () {
      expect(calculator.calculate('add', "1", 2)).toBe(3);
    });
    it('má akci pro odčítání', function () {
      expect(calculator.calculate('sub', "2", 1)).toBe(1);
    });

  });

});