
var Generator = function(initialNumber) {
  this.initialNumber = initialNumber || 0;
};

Generator.prototype.increase  = function() {
  return this.initialNumber++;
}

var generatorFrom10 = new Generator(10);
var firstGenerated = generatorFrom10.increase();