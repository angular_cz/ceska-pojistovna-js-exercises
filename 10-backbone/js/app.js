var app = app || {};

var AppRouter = Backbone.Router.extend({
  routes: {
    'users/': 'list',
    'users/:id': 'edit',
    '*path': 'otherwise'
  },

  otherwise: function () {
    Backbone.history.navigate('users');
    this.list();
  },

  showView_: function (view) {

    if (this.currentView) {
      this.currentView.remove();
    }
    this.currentView = view;

    view.render();
    $('#userApp').html(view.el);
  },

  list: function () {
    var listUsers = new app.UserList();

    var listUsersView = new app.ListUsersView({collection: listUsers});
    this.showView_(listUsersView);

    listUsers.fetch({reset: true});
  },

  edit: function (id) {
    var user = new app.User({id: id});

    var editUserView = new app.EditUserView({model: user});
    this.showView_(editUserView);

    user.fetch();
  }
});

$(function () {
  app.router = new AppRouter();
  Backbone.history.start();
});

