var app = app || {};

app.UserList = Backbone.Collection.extend({
  url: '/api/users',
  model: app.User,

  comparator: function (item) {
    return item.get('name').toLowerCase();
  }
});