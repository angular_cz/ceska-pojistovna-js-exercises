var app = app || {};

app.ListUsersView = Backbone.View.extend({
  template: _.template($('#list-users-template').html()),

  initialize: function () {
    this.listenTo(this.collection, 'sort', this.addAll);
    this.listenTo(this.collection, 'add', this.addOne);
    this.listenTo(this.collection, 'reset', this.addAll);
  },

  render: function () {
    this.$el.html(this.template());
    this.$list = this.$('#user-list');

    this.renderForm();
  },

  renderForm: function () {
    var formView = new app.UserFormView({
        el: this.$('#create-form'),
        model: new app.User()
      }
    );

    formView.hideCancel();
    formView.on('save', function (model) {
      this.collection.create(model.toJSON(), {wait: true});

      model.set({
        name: '',
        email: '',
        archived: false
      });
    }.bind(this));

    formView.render();
  },

  addAll: function () {
    this.$list.html('');
    this.collection.forEach(this.addOne, this);
  },
  addOne: function (item) {
    var userView = new app.UserItemView({model: item});
    userView.render();

    this.$list.append(userView.el);
  }
});
