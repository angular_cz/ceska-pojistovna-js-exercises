var App = Ember.Application.create();

App.Router.map(function () {
  this.route('missing', { path: "/*path"})

  this.route('users', {path: '/users'});
  this.route('user', {path: '/users/:id'});
});

App.MissingRoute = Ember.Route.extend({
  redirect: function () {
    this.transitionTo('users');
  }
});

App.IndexRoute = App.MissingRoute.extend({});

App.UsersRoute = Ember.Route.extend({
  model: function () {
    return this.store.findAll('user');
  }
});

App.UserRoute = Ember.Route.extend({
  model: function (params) {
    return this.store.find('user', params.id);
  }
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
  host: '/api/wrapped',

  shouldReloadAll:function() {
    return false;
  },
  shouldBackgroundReloadRecord : function() {
    return false;
  }
});
