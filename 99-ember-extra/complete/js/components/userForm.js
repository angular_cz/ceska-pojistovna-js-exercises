
App.UserFormComponent = Ember.Component.extend({
  actions: {
    save: function () {
      this.sendAction('submit', this.get('model'));
    }
  },

  invalid: Ember.computed('validation', function () {
    var validation = this.get('validation');
    return validation.name || validation.email;
  }),

  validation: Ember.computed('model.email', 'model.name', function () {
    var errors = {};

    var name = this.get('model.name');
    if (!name) {
      errors.name = 'Musíte zadat jméno';
    }

    var email = this.get('model.email');
    if (!email) {
      errors.email = 'Musíte zadat email.';
    } else {
      var emailPattern = /^[a-z0-9!#$%&'*+\/=?^_`{|}~.-]+@[a-z0-9]([a-z0-9-]*[a-z0-9])?(\.[a-z0-9]([a-z0-9-]*[a-z0-9])?)*$/i;
      if (!emailPattern.test(email)) {
        errors.email = 'Email není platný.';
      }
    }

    return errors;
  })
});