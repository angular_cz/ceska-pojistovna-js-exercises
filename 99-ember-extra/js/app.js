// TODO 1.1 - Inicializujte aplikaci
var App = {};

App.Router.map(function () {
  this.route('missing', { path: "/*path"})

  this.route('users', {path: '/users'});

  // TODO 4.1 - Vytvořte routu pro editaci
});

App.MissingRoute = Ember.Route.extend({
  redirect: function () {
    // TODO 1.3 - Přesměrujte na routu users
  }
});

App.IndexRoute = App.MissingRoute.extend({});

App.UsersRoute = Ember.Route.extend({
 // TODO 2.1 Načtěte model ze zdroje users

});

App.UserRoute = Ember.Route.extend({
  model: function (params) {
    return this.store.find('user', params.id);
  }
});

App.ApplicationAdapter = DS.RESTAdapter.extend({
  host: '/api/wrapped',

  shouldReloadAll:function() {
    return false;
  },
  shouldBackgroundReloadRecord : function() {
    return false;
  }
});
