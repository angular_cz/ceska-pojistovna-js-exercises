App.UsersController = Ember.Controller.extend({
  sortProperties: ['name'],
  sortedUsers: Ember.computed.sort('model', 'sortProperties'),

  init: function () {
    var emptyObject = Ember.Object.create();
    this.set('newUser', emptyObject);
  },

  actions: {
    createUser: function (newUser) {
      var userData = {
        name:  newUser.get('name'),
        email: newUser.get('email'),
        archived : newUser.get('archived')
      };

      // TODO 7.2 - Uložte záznam

      this.init();
    }
  }
});