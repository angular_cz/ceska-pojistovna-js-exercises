package cz.angular.user;

import javax.net.ssl.SSLEngineResult;
import java.util.Date;

public class User {
  private long id;
  private String name;
  private String email;
  private boolean archived;
  private Date date;

  public User(Long id,String name, String email, boolean archived) {
    this();
    this.id = id;
    this.name = name;
    this.email = email;
    this.archived = archived;
  }

  public User() {
    this.date = new Date();
  }

  public long getId() {
    return id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public boolean isArchived() {
    return archived;
  }

  public void setArchived(boolean archived) {
    this.archived = archived;
  }

  public Date getDate() {
    return date;
  }

  public void setDate(Date date) {
    this.date = date;
  }
}
