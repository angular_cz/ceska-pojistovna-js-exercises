package cz.angular.user;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

public class UserService implements IUserService {

	private HashMap<Long, User> users;
  private Long idSeq;
  public UserService() {
		users = new HashMap<Long,User>();

    idSeq = 0L;

    createUser("Petr Novák", "petr@novak.cz", false);
    createUser("Jaroslav Novák", "jarek@novak.cz", false);
    createUser("Martin Novák", "martin@novak.cz", false);
    createUser("Karolína Nováková", "karolina@novak.cz", true);
    createUser("Tamara Nováková", "tamara@novak.cz", false);
  }

  private User createUser(String name, String email, boolean archived) {
    idSeq++;
    User user = new User(idSeq, name, email, archived);
    users.put(user.getId(), user);
    return user;
  }

  @Override
  public Collection<User> getUsers() {
		return users.values();
	}

  @Override
  public User getUser(Long id) {

    if (!users.containsKey(id)) {
      throw new IndexOutOfBoundsException();
    }

    return users.get(id);
  }

  @Override
  public void removeUser(Long id) {
    if (!users.containsKey(id)) {
      throw new IndexOutOfBoundsException();
    }

    users.remove(id);
  }

  @Override
  public User updateUser(Long id, User user) {
    User savedUser = users.get(id);
    savedUser.setName(user.getName());
    savedUser.setEmail(user.getEmail());
    savedUser.setArchived(user.isArchived());

    return savedUser;
  }

  @Override
  public User createUser(User user) {
    return createUser(user.getName(), user.getEmail(), user.isArchived());
  }
}
