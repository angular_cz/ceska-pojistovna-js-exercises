package cz.angular.user;

import java.util.Collection;

/**
 * Created by vita on 22.08.15.
 */
public interface IUserService {
  Collection<User> getUsers();

  User getUser(Long id);

  void removeUser(Long id);

  User updateUser(Long id, User user);

  User createUser(User user);
}
