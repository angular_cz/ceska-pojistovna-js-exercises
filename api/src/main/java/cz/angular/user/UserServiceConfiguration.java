package cz.angular.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

/**
 * Created by vita on 22.08.15.
 */
@Configuration
public class UserServiceConfiguration {
  @Bean
  UserService userNoSessionService() {
    return new UserService();
  }

  @Scope(value = "session", proxyMode = ScopedProxyMode.TARGET_CLASS)
  @Bean
  UserService userSessionService() {
    return new UserService();
  }
}
