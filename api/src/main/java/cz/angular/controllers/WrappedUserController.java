package cz.angular.controllers;

import cz.angular.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value= "/wrapped")
public class WrappedUserController extends AbstractWrappedUserController {

  @Autowired
  public WrappedUserController(@Qualifier("userSessionService") UserService userService) {
    this.userService = userService;
  }


}