package cz.angular.controllers;

import cz.angular.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping(value= "/no-session")
public class UserNoSessionController extends AbstractUserController {


  @Autowired
  public UserNoSessionController(@Qualifier("userNoSessionService") UserService userService) {
    this.userService = userService;
  }


}