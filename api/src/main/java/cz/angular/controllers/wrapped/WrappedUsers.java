package cz.angular.controllers.wrapped;

import cz.angular.user.User;

import java.util.Collection;

/**
 * Created by vita on 01.08.15.
 */
public class WrappedUsers {
  private Collection<User> users;

  public static WrappedUsers valueOf(Collection<User> users) {
    return new WrappedUsers(users);
  }

  public WrappedUsers(Collection<User> users) {
    this.users = users;
  }

  public Collection<User> getUsers() {
    return users;
  }

  public void setUsers(Collection<User> users) {
    this.users = users;
  }
}
