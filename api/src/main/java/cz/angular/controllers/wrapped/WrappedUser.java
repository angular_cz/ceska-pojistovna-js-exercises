package cz.angular.controllers.wrapped;

import cz.angular.user.User;

/**
 * Created by vita on 01.08.15.
 */
public class WrappedUser {
  private User user;

  public WrappedUser() {
  }

  public static WrappedUser valueOf(User user) {
    return new WrappedUser(user);
  }

  public WrappedUser(User user) {
    this.user = user;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }
}
