package cz.angular.controllers;

import cz.angular.controllers.wrapped.Empty;
import cz.angular.controllers.wrapped.WrappedUser;
import cz.angular.controllers.wrapped.WrappedUsers;
import cz.angular.user.User;
import cz.angular.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Collection;

public class AbstractWrappedUserController {

  public static final Empty EMPTY = new Empty();

  protected UserService userService;

  @RequestMapping(value = "/users", method= RequestMethod.GET)
  public WrappedUsers getUsers() {
    return nest(userService.getUsers());
  }

  @RequestMapping(value = "/users", method= RequestMethod.POST)
  public WrappedUser createUser(@RequestBody WrappedUser user) {
    return nest(userService.createUser(user.getUser()));
  }

  @RequestMapping(value = "/users/{id}", method= RequestMethod.GET)
  public WrappedUser getUser(@PathVariable Long id) {
    return nest(userService.getUser(id));
  }

  @RequestMapping(value = "/users/{id}", method= RequestMethod.DELETE)
  public Empty removeUser(@PathVariable Long id) {
    userService.removeUser(id);
    return EMPTY;
  }

  @RequestMapping(value = "/users/{id}", method = {RequestMethod.POST, RequestMethod.PUT})
  public WrappedUser updateUser(@PathVariable Long id, @RequestBody WrappedUser user) {
    return nest(userService.updateUser(id, user.getUser()));
  }

  private WrappedUsers nest(Collection<User> users) {
    return WrappedUsers.valueOf(users);
  }

  private WrappedUser nest(User user) {
    return WrappedUser.valueOf(user);
  }

}