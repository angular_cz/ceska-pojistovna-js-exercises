package cz.angular.controllers;

import cz.angular.user.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

@RestController
@RequestMapping(value= "/")
public class UserSessionController extends AbstractUserController {

  @Autowired
  public UserSessionController(@Qualifier("userSessionService") UserService userService) {
    this.userService = userService;
  }
}