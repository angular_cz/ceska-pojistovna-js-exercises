
function Calculator() {
  this.operations = {};

  this.addOperation = function (operation) {
    // TODO 1.2 - Ošetření přidání neplatné operace

    this.operations[operation.name] = operation;
  };

  this.calculate = function (name, a, b) {
    // TODO 3 - Ošetření neznámé operace

    return this.operations[name].calculate(a, b);
  }
}

// TODO 2 - Vytvořte výjimku UnknownOperationError


// ---------------------------------------------------
// Operation
function Operation() {
  this.name = this.constructor.name.toLowerCase();
}

Operation.prototype.calculate = function (a, b) {
  return this.calculateInternal_(parseFloat(a), parseFloat(b));
};

// Add

function Add() {
  Operation.call(this);
}

Add.prototype = Object.create(Operation.prototype);
Add.prototype.constructor = Add;

Add.prototype.calculateInternal_ = function (a, b) {
  return a + b;
};
