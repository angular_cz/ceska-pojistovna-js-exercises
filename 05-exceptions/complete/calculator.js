
function Calculator() {
  this.operations = {};

  this.addOperation = function (operation) {
    if(!(operation instanceof Operation)){
      throw new TypeError("operation must be instance of Operation");
    }
    this.operations[operation.name] = operation;
  };

  this.calculate = function (name, a, b) {
    if (!this.operations[name]) {
      throw new UnknownOperationError('Unknown operation: ' + name);
    }

    return this.operations[name].calculate(a, b);
  }
}

var UnknownOperationError = function(message) {
  this.name = 'UnknownOperationError';
  this.message = message || 'Unknown operation';
  this.stack = (new Error()).stack;
};

UnknownOperationError.prototype = Object.create(Error.prototype);
UnknownOperationError.prototype.constructor = UnknownOperationError;

// Operation

function Operation() {
  this.name = this.constructor.name.toLowerCase();
}

Operation.prototype.calculate = function (a, b) {
  return this.calculateInternal_(parseFloat(a), parseFloat(b));
};

// Add

function Add() {
  Operation.call(this);
}

Add.prototype = Object.create(Operation.prototype);
Add.prototype.constructor = Add;

Add.prototype.calculateInternal_ = function (a, b) {
  return a + b;
};
