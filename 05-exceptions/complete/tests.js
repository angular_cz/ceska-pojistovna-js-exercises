describe('Kalkulačka', function () {

  it('může být instancována', function () {
    expect((new Calculator()) instanceof Calculator).toBeTruthy();
  });

  it('může mít přidánu operaci pod jménem', function () {
    var calc = new Calculator();
    var operation = new Operation();
    operation.name = 'op';

    calc.addOperation(operation);

    expect(calc.operations['op']).toBe(operation);
  });

  it('umožuje provolat operaci', function () {
    var calc = new Calculator();
    var operation = new Operation();
    operation.name = 'op';

    spyOn(operation, "calculate").and.returnValue(42);

    calc.addOperation(operation);

    expect(calc.calculate('op', 40, 2)).toBe(42);
    expect(operation.calculate).toHaveBeenCalledWith(40, 2);
  });

  describe('vyhodí výjimku', function () {
    it('TypeError, při pokusu o přidání akce, která není potomkem Operation (TODO 1)', function () {
      var calculator = new Calculator();

      expect(
        function () {
          calculator.addOperation({});
        }
      ).toThrowError(TypeError);
    });

    it('UnknownOperationError, při volání neznámé operace (TODO 3)', function () {
      var calculator = new Calculator();
      expect(function () {
        calculator.calculate("unknown")
      }).toThrowError(UnknownOperationError);
    });

    it('s názvem operace, při volání neznámé operace (TODO 3)', function () {
      var calculator = new Calculator();

      function throwFunction() {
        calculator.calculate("unknown-op")
      }
      expect(throwFunction).toThrowError(UnknownOperationError);
      expect(throwFunction).toThrowError(/unknown-op/);
    });
  });

});

describe('Výjimka UnknownOperationError (TODO 2)', function () {
  it('může být instancována', function () {
    var error = new UnknownOperationError();

    expect(error instanceof UnknownOperationError).toBeTruthy();
  });

  it('má správný name', function () {
    var error = new UnknownOperationError();

    expect(error.name).toBe('UnknownOperationError');
  });

  it('má přiřazený stack', function () {
    var error = new UnknownOperationError();

    expect(error.stack).toBeDefined();
  });

  it('obsahuje předanou message', function () {
    var throwFunction = function () {
      throw new UnknownOperationError("my message");
    };

    expect(throwFunction).toThrowError(UnknownOperationError, "my message");
  });

  it('je potomkem Error', function () {
    var error = new UnknownOperationError();

    expect(error instanceof Error).toBeTruthy();
  });

  it('má svůj konstruktor', function () {
    var error = new UnknownOperationError();
    expect(error.constructor).toBe(UnknownOperationError);
  });

});